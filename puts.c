#include "std.h"

int puts(const char* str){

	for(;str[0] != '\0'; str++)
		asm volatile(
			"movq %0, %%rax	\n\t"
			"movq %1, %%rdi	\n\t"
			"movq %2, %%rsi	\n\t"
			"movq $1, %%rdx	\n\t"
			"syscall		\n\t"
			: 
			: "i" (SYSCALL_WRITE), "i" (stdout), "m" (str)
		);

	asm volatile(
		"movq %0, %%rax		\n\t"
		"movq %1, %%rdi		\n\t"
		"movq $14, %%rsi	\n\t"
		"movq $1, %%rdx		\n\t"
		"syscall			\n\t"
		: 
		: "i" (SYSCALL_WRITE), "i" (stdout)
	);

	return 0;
}
