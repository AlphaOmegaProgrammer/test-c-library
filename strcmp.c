#include "std.h"

int strcmp(char* str1, char* str2){
	unsigned long long ctr = 0;
	int diff;

	if(str1 == NULL || str2 == NULL)
		return 1;

	for(; str1[ctr] != '\0'; ctr++){
		diff = str1[ctr] - str2[ctr];
		if(diff != 0)
			return diff;
	}

	return str1[ctr] - str2[ctr];
}
