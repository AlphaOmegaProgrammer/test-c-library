CC=gcc
CFLAGS=-g -Wall -Wextra -pedantic #-O3 -march=native 

all: chdir exit getchar malloc memset puts strcmp strcpy strlen strtok \
	execvp fork waitpid


chdir:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC chdir.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared chdir.o -o lib-chdir.so

getchar:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC getchar.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared getchar.o -o lib-getchar.so

exit:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC exit.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared exit.o -o lib-exit.so

malloc:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC malloc.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared malloc.o -o lib-malloc.so

memset:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC memset.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared memset.o -o lib-memset.so

puts:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC puts.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared puts.o -o lib-puts.so

strcmp:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC strcmp.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared strcmp.o -o lib-strcmp.so

strcpy:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC strcpy.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared strcpy.o -o lib-strcpy.so

strlen:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC strlen.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared strlen.o -o lib-strlen.so

strtok:
	$(CC) $(CFLAGS) -nostdlib -nostdinc -c -fPIC strtok.c
	$(CC) $(CFLAGS) -nostdlib -nostdinc -shared strtok.o -o lib-strtok.so



execvp:
	$(CC) $(CFLAGS) -nostdlib -c -fPIC execvp.c
	$(CC) $(CFLAGS) -nostdlib -shared execvp.o -o lib-execvp.so

fork:
	$(CC) $(CFLAGS) -nostdlib -c -fPIC fork.c
	$(CC) $(CFLAGS) -nostdlib -shared fork.o -o lib-fork.so

waitpid:
	$(CC) $(CFLAGS) -nostdlib -c -fPIC waitpid.c
	$(CC) $(CFLAGS) -nostdlib -shared waitpid.o -o lib-waitpid.so



clean:
	rm *.o *.so
