#include "std.h"

int chdir(const char* path){
	asm volatile(
		"movq %0, %%rax	\n\t"
		"movq %1, %%rdi	\n\t"
		"syscall		\n\t"
		:
		: "i" (SYSCALL_CHDIR), "m" (path)
	);

	return 0;
}
