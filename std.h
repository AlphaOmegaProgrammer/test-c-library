#include "syscall_table.h"

#define EOF (-1)
#define NULL (0)

typedef int pid_t;
typedef unsigned long size_t;

#define stdin 0
#define stdout 1
#define stderr 2
