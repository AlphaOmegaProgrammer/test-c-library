#include "memset.h"

void* memset(void* destp, int c, size_t size){
	size_t ctr;
	unsigned char *dest = destp;

	for(ctr=0; ctr<size; ctr++)
		dest[ctr] = c;

	return destp;
}
