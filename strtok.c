#include "std.h"

char* strtok(char* str, const char *delim){
	static char *start = NULL;
	if(str == NULL && start == NULL)
		return NULL;

	if(str != NULL)
		start = str;

	char* end = start;
	size_t cmp_length;
	for(;;end++){
		for(cmp_length = 0; end[cmp_length] == delim[cmp_length] && end[cmp_length] != '\0'; cmp_length++){}

		if(delim[cmp_length] == '\0')
			end[0] = '\0';

		if(end[0] == '\0')
			break;
	}

	char* ret = start;
	start = end + cmp_length;

	if(delim[cmp_length] != '\0')
		start = NULL;

	return ret;
}
