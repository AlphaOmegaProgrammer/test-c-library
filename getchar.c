#include "std.h"

int getchar(){
	char result;
	long _ret = 0;

	register long _num asm("rax") = (SYSCALL_READ);
	register long _arg1 asm("rdi") = 0;
	register long _arg2 asm("rsi") = (long)&result;
	register long _arg3 asm("rdx") = (long)(1);

	asm volatile(
		"syscall\n"
		: "=a"(_ret)
		: "r"(_arg1), "r"(_arg2), "r"(_arg3), "0"(_num)
	);

	return (int)result;
}
