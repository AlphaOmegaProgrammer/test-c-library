#define SYSCALL_READ	(0)
#define SYSCALL_WRITE	(1)

#define SYSCALL_MMAP	(9)

#define SYSCALL_GETPID	(39)

#define SYSCALL_FORK	(57)

#define SYSCALL_EXECVE	(59)
#define SYSCALL_EXIT	(60)

#define SYSCALL_CHDIR	(80)
