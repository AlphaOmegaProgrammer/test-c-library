//#include "std.h"

#define SYSCALL_EXECVE (59)

#include <stdio.h>
#include <stdlib.h>

int execvp(const char *file, char *const argv[]){
	char *environment[1];
	environment[0] = NULL;

	char path[256] = {0}, *p;
	path[0] = '/';
	path[1] = 'u';
	path[2] = 's';
	path[3] = 'r';
	path[4] = '/';
	path[5] = 'b';
	path[6] = 'i';
	path[7] = 'n';
	path[8] = '/';

	unsigned char ctr = 0;
	for(;;ctr++){
		path[ctr + 9] = file[ctr];
		if(file[ctr] == '\0')
			break;
	}

	int ret;
	p = &path[9];
	asm volatile(
		"movq %1, %%rax	\n\t"
		"movq %2, %%rdi	\n\t"
		"movq %3, %%rsi	\n\t"
		"syscall		\n\t"
		"movq %0, %%rax	\n\t"
		: "=m" (ret)
		: "i" (SYSCALL_EXECVE), "m" (p), "m" (argv), "m" (environment)
	);

	if(ret == -1){
		p = &path[4];
		asm volatile(
			"movq %1, %%rax	\n\t"
			"movq %2, %%rdi	\n\t"
			"movq %3, %%rsi	\n\t"
			"syscall		\n\t"
			"movq %0, %%rax	\n\t"
			: "=m" (ret)
			: "i" (SYSCALL_EXECVE), "m" (p), "m" (argv), "m" (environment)
		);
	}

	if(ret == -1){
		asm volatile(
			"movq %1, %%rax	\n\t"
			"movq %2, %%rdi	\n\t"
			"movq %3, %%rsi	\n\t"
			"syscall		\n\t"
			"movq %0, %%rax	\n\t"
			: "=m" (ret)
			: "i" (SYSCALL_EXECVE), "m" (path), "m" (argv), "m" (environment)
		);
	}

	return ret;
}
