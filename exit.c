#include "std.h"

void exit(int status){
	asm volatile(
		"movq %0, %%rax	\n\t"
		"movq %1, %%rdi	\n\t"
		"syscall		\n\t"
		:
		: "i" (SYSCALL_EXIT), "m" (status)
	);
}
