#include "std.h"

void* malloc(size_t bytes){
	void *ret;

	asm volatile(
		"movq %1, %%rax		\n\t"
		"xor %%rdi,%%rdi	\n\t"
		"movq %2, %%rsi		\n\t"
		"movq $7, %%rdx		\n\t"
		"movq $34, %%r10	\n\t"
		"xor %%r8, %%r8		\n\t"
		"xor %%r9, %%r9		\n\t"
		"syscall			\n\t"
		"movq %%rax, %0		\n\t"
		: "=m" (ret)
		: "i" (SYSCALL_MMAP), "m" (bytes)
	);

	return ret;
}
